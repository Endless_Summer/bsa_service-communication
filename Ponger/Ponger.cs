﻿using RabbitMQ.Wrapper;
using System;
using System.Threading.Tasks;

namespace Ponger
{
    class Ponger
    {
        public static void Main()
        {
            Console.WriteLine(" Ponger started!");

            try
            {
                using RabbitMQWrapper rabbit = new RabbitMQWrapper("ping_queue", "pong_queue", "pong");

                Task.Run(rabbit.ListenQueue);

                Console.WriteLine(" Press [Enter] to exit.\n");
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error: " + e.Message);
                Console.ResetColor();
            }

        }

    }
}
