﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQ.Wrapper
{
    public class RabbitMQWrapper : IDisposable
    {
        readonly ConnectionFactory factory;
        readonly IConnection connection;
        readonly IModel model;
        readonly string listenQueueName;
        readonly string sendQueueName;
        readonly string message;

        public RabbitMQWrapper(string sendQueueName, string listenQueueName, string message)
        {
            this.listenQueueName = listenQueueName;
            this.sendQueueName = sendQueueName;
            this.message = message;

            factory = new ConnectionFactory
            {
                UserName = "guest", // default
                Password = "guest", // default
                VirtualHost = "/",  // default
                HostName = "localhost"
            };

            connection = factory.CreateConnection();
            
            model = connection.CreateModel();
            model?.QueueDeclare(queue: listenQueueName, durable: false, exclusive: false, autoDelete: false, arguments: null);
            model?.QueueDeclare(queue: sendQueueName, durable: false, exclusive: false, autoDelete: false, arguments: null);

        }

        public void SendMessageToQueue()
        {
            var body = Encoding.UTF8.GetBytes(message);

            model.ConfirmSelect();
            model.BasicPublish(exchange: "", routingKey: sendQueueName, basicProperties: null, body: body);
            model.WaitForConfirmsOrDie(TimeSpan.FromSeconds(5));

            WriteText(message, ConsoleColor.Yellow, false);
            Console.WriteLine($" sent at {DateTime.Now}");
        }

        public void ListenQueue()
        {
            var consumer = new EventingBasicConsumer(model);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var originalMessage = Encoding.UTF8.GetString(body);

                WriteText(originalMessage, ConsoleColor.Green, false);
                Console.WriteLine($" received at {DateTime.Now}");

                Task.Delay(2500).Wait();

                SendMessageToQueue();

            };
            model.BasicConsume(queue: listenQueueName, autoAck: true, consumer: consumer);

        }

        public void QueuePurge()
        {
            model?.QueuePurge(listenQueueName);
            model?.QueuePurge(sendQueueName);
        }

        private void WriteText(string text, ConsoleColor color, bool isLine = true)
        {
            Console.ForegroundColor = color;

            if (isLine)
                Console.WriteLine(text);
            else
                Console.Write(text);

            Console.ForegroundColor = color;
            Console.ResetColor();
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    model?.Close();
                }

                connection?.Close();

                disposedValue = true;
                //Console.WriteLine($"Dispose {listenQueueName}");
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

    }
}
