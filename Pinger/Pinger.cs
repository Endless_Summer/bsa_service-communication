﻿using RabbitMQ.Wrapper;
using System;
using System.Threading.Tasks;

namespace Pinger
{
    class Pinger
    {
        public static void Main()
        {
            Console.WriteLine(" Pinger started!");

            try
            {
                using RabbitMQWrapper rabbit = new RabbitMQWrapper("pong_queue", "ping_queue", "ping");

                Console.WriteLine(" Press [Enter] to exit.\n");

                rabbit.QueuePurge(); // Очищаем очередь. В игре "мяч" должен быть один.
                rabbit.SendMessageToQueue(); // Первый разовый запуск "мяча"

                Task.Run(rabbit.ListenQueue);

                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error: " + e.Message);
                Console.ResetColor();
            }

        }
    }
}
